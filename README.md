### TestCase
1. Build. Compile the latest version of Nginx server with a lua-nginx-module.
    - Login to Gitlab CI - some.secret.link (login and password - contact some.secret.user on
telegram)
    - Create repository with name: Surname_Name
    - Create dockerfile
    - Create nginx.conf
    - Create custom index.html

2. Push docker image to the public Docker registry.

3. Deploy. Deploy Nginx container on EC2 instance using docker-machine.
    - Create gitlab-ci.yaml with stages: build, deploy
    - Push the final code revision to a public Gitlab repository and share URL link to it.

### DOCKER_HOST
DOCKER_HOST="tcp://ec2-18-191-81-201.us-east-2.compute.amazonaws.com:2375"